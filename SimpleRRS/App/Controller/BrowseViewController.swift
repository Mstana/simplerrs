//
//  BrowseViewController.swift
//  SimpleRRS
//
//  Created by marek on 28/06/2018.
//  Copyright © 2018 stana. All rights reserved.
//

import UIKit
import WebKit

var browserContext = 0

class BrowseViewController : UIViewController {
    
    var webView: WKWebView!
    var progressView: UIProgressView!
    var url: URL!
    
    private let progressKeyPath = "estimatedProgress"
    
    init(url:URL) {
        super.init(nibName: nil, bundle: nil)
        self.url = url
    }
    
    init(url:URL, title:String?) {
        super.init(nibName: nil, bundle: nil)
        self.url = url
        if let safeString = title {
            self.title = safeString
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func loadView() {
        
        webView = WKWebView()
        webView.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        webView.navigationDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        webView.addObserver(self, forKeyPath: progressKeyPath, options: .new, context: &browserContext)
        view = webView
        

        progressView = UIProgressView(progressViewStyle: .default)
        progressView.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        progressView.tintColor = #colorLiteral(red: 0.6576176882, green: 0.7789518833, blue: 0.2271372974, alpha: 1)
        navigationController?.navigationBar.addSubview(progressView)
        let navigationBarBounds = self.navigationController?.navigationBar.bounds
        progressView.frame = CGRect(x: 0, y: navigationBarBounds!.size.height - 2, width: navigationBarBounds!.size.width, height: 2)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.prefersLargeTitles = false

        webView.load(URLRequest(url: url))
    }
    
    deinit {
        webView.removeObserver(self, forKeyPath: progressKeyPath)
        progressView.removeFromSuperview()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        guard let change = change else { return }
        if context != &browserContext {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
     
        if keyPath == "estimatedProgress" {
            if let progress = (change[NSKeyValueChangeKey.newKey] as AnyObject).floatValue {
                progressView.progress = progress;
            }
            return
        }
    }
}

// Mark: Web Navigation Delegate
extension BrowseViewController : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        progressView.isHidden = true
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        progressView.isHidden = false
    }
}
