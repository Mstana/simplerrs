//
//  ViewController.swift
//  SimpleRRS
//
//  Created by marek on 26/06/2018.
//  Copyright © 2018 stana. All rights reserved.
//

import UIKit
import Kingfisher


class ArticlesListViewController: UIViewController {

    var tableView: UITableView!
    var emptyTableViewHolder: UIView!
    let interactor = Interactor()

    var articles: [Article] = Array()
    
    var showingCategory: NewsCagetory = .general
    var showingCountry: String = UserSettings.shared.country
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    
    private static let kArticleCellIdentifier = "articleTableViewCell"
    
    override func loadView() {
        super.loadView()
        
        view.backgroundColor = .white
        
        tableView = UITableView(frame: self.view.frame)
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ArticleTableViewCell.classForCoder(), forCellReuseIdentifier: ArticlesListViewController.kArticleCellIdentifier)
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView(frame:.zero)
        tableView.refreshControl = refreshControl
        tableView.estimatedRowHeight = 240
        tableView.backgroundColor = .clear
        self.view.addSubview(tableView)
        
        let categoriesView = CategoriesScrollView(frame:CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
        categoriesView.categories = NewsCagetory.values
        categoriesView.selected = showingCategory
        categoriesView.categoriesDelegate = self
        tableView.tableHeaderView = categoriesView
        
        emptyTableViewHolder = UIView(frame:self.view.frame)
        emptyTableViewHolder.alpha = 0 // use alpha instead of isHidden for animation later on
        emptyTableViewHolder.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        emptyTableViewHolder.isUserInteractionEnabled = false
        self.view.insertSubview(emptyTableViewHolder, belowSubview: tableView)
        
        let label = PaddingLabel(frame: emptyTableViewHolder.bounds)
        label.isUserInteractionEnabled = false
        label.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin]
        label.textInsets = UIEdgeInsets(top: 32, left: 32, bottom: 32, right: 32)
        label.textAlignment = .center
        label.text = NSLocalizedString("No results found", comment: "Empty table view placeholder")
        label.textColor = .darkText
        label.font = UIFont.systemFont(ofSize: 24, weight: .medium)
        label.sizeToFit()
        label.center = emptyTableViewHolder.center
        emptyTableViewHolder.addSubview(label)
        
        let image = UIImageView(image: UIImage(named: "empty-table-image")?.withRenderingMode(.alwaysTemplate))
        image.tintColor = .darkGray
        image.center = CGPoint(x: emptyTableViewHolder.frame.width/2, y: label.frame.origin.y - image.frame.height)
        emptyTableViewHolder.addSubview(image)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.prefersLargeTitles = true
        
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: tableView)
        }
        
        self.fetchData()
        self.updateNavigationItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func fetchData() {
        self.fetchDataForPageIndex(pageIndex: 0)
    }
    
    func fetchDataForPageIndex(pageIndex:Int) {
        ArticlesManager.shared.fetchTopArticlesForCountry(showingCountry, category: showingCategory, page: pageIndex+1) { (articles, error) in
            
            guard error == nil else {
                
                let alert = UIAlertController(title: NSLocalizedString("Download error", comment: "Alert title"), message: error?.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Alert button title"), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return;
            }
            
            if pageIndex < 2 {
                self.articles = []
            }
            
            self.articles = self.articles + articles
            
            if pageIndex < 1 || articles.count != 0 {
                self.tableView.reloadData()
            }
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func updateNavigationItem() {
        navigationItem.title = UserSettings.shared.titleFor(newsCategory: showingCategory)
        
        var title = showingCountry.uppercased()
        if showingCountry == "" {
            title = NSLocalizedString("World", comment:"Navigation Button item title")
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(presentRegionsAlertController))
    }
}

// MARK: - Table View Delegate
extension ArticlesListViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return articles[indexPath.row].hasPhoto ? 240 : 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = FTArticleDetailViewController(article: articles[indexPath.row])
        vc.transitioningDelegate = self
        vc.interactor = interactor
        vc.modalPresentationStyle = .fullScreen // .custom breaks dismiss animation
        vc.strongReference = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView == tableView) {
            for cell in tableView.visibleCells {
                guard let articleCell:ArticleTableViewCell = cell as? ArticleTableViewCell
                    else {
                        continue
                }
                if articleCell.cellState == .perex {
                    articleCell.cellState = .title
                    UIView.animate(withDuration: 0.3) {
                        // Because build in animation is slow
                        articleCell.contentHolder.setContentOffset(.zero, animated: false)
                    }
                }
            }
        }
    }
}

// MARK: - Table View Data Source
extension ArticlesListViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticlesListViewController.kArticleCellIdentifier, for: indexPath) as! ArticleTableViewCell
        cell.article = articles[indexPath.row]
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if articles.last == articles[indexPath.row] {
            let pageIndex = articles.count/20;
            fetchDataForPageIndex(pageIndex: pageIndex)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        UIView.animate(withDuration: 0.3) {
            self.emptyTableViewHolder?.alpha = (self.articles.count == 0) ? 1 :0
        }
        return articles.count
    }
}

// MARK: - Article Cell Delegate
extension ArticlesListViewController : ArticleTableViewCellDelegate {
    
    func articleCellRequestedActionForArticle(_ article: Article?) {
        if let safeURL = article?.URL {
            let vc = BrowseViewController(url: URL(string: safeURL)!, title:article?.sourceName)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

// MARK: - Actions
extension ArticlesListViewController {
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        fetchData()
    }
    
    @objc func presentRegionsAlertController() {
        let alert = UIAlertController(title: NSLocalizedString("Choose region", comment: "Alert sheet title"), message: nil, preferredStyle: .actionSheet)
        
        if showingCountry != "" {
            alert.addAction(UIAlertAction(title: NSLocalizedString("World wide", comment: "Alert button title"), style: .default, handler: { (action) in
                self.showingCountry = ""
                UserDefaults.standard.set("", forKey: Constants.userCountry)
                self.fetchData()
                self.updateNavigationItem()
            }))
        }
        
        let locale = NSLocale.current
        
        
        let sortedCountries = UserSettings.shared.supportedCountries.sorted { (first, second) -> Bool in
            guard let safeFirst = locale.localizedString(forRegionCode: first),
                  let safeSecond = locale.localizedString(forRegionCode: second) else {
                return false
            }
            
            return safeFirst < safeSecond
        }
        
        for region in sortedCountries {
            
            guard (region != self.showingCountry) else { continue }
            
            alert.addAction(UIAlertAction(title: locale.localizedString(forRegionCode: region), style: .default, handler: { (action) in
                self.showingCountry = region
                UserDefaults.standard.set(region, forKey: Constants.userCountry)
                self.fetchData()
                self.updateNavigationItem()
            }))
        }
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Alert sheet button title"), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: Force Touch on cell
extension ArticlesListViewController: UIViewControllerPreviewingDelegate {
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        if let indexPath = tableView.indexPathForRow(at: location) {
            return viewControllerForArticle(article: articles[indexPath.row])
        }
        return nil
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        let ftArticleVC : FTArticleDetailViewController = (viewControllerToCommit as? FTArticleDetailViewController)!
        let article = ftArticleVC.article
        
        let vc = BrowseViewController(url:URL(string:(article?.URL)!)!, title: article?.sourceName)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func touchedView(view: UIView, location: CGPoint) -> Bool {
        let locationInView = view.convert(location, from: tableView)
        return view.bounds.contains(locationInView)
    }
    
    private func viewControllerForArticle(article: Article) -> UIViewController {
        return FTArticleDetailViewController(article: article)
    }
}

// MARK: Transitioning Delegate
extension ArticlesListViewController : UIViewControllerTransitioningDelegate {
 
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactor.hasStarted ? interactor : nil
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ArticleDetailDismissAnimator()
    }
}

// MARK: Categories Scroll View Delegate
extension ArticlesListViewController : CategoriesScrollViewDelegate {
    
    func categoriesScrollViewDidSelectNewsCategory(_ category: NewsCagetory) {
        
        self.showingCategory = category
        self.fetchData()
        self.updateNavigationItem()
    }
}
