//
//  FTArticleDetailViewController.swift
//  SimpleRRS
//
//  Created by marek on 28/06/2018.
//  Copyright © 2018 stana. All rights reserved.
//

import UIKit


class FTArticleDetailViewController : UIViewController {
    
    var article: Article!
    
    var photoImageView: UIImageView!
    var titleTextView: UITextView!
    var infoLabel: PaddingLabel!
    var panGesture:UIPanGestureRecognizer!
    var interactor:Interactor? = nil
    var dragView:UIView!

    var attributedMainLabelString: NSMutableAttributedString!
    var attributedBottomLabelString: NSMutableAttributedString!
    
    var strongReference: UIViewController!
    
    init(article:Article) {
        super.init(nibName: nil, bundle: nil)
        
        self.article = article
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func loadView() {
        super.loadView()
        
        self.view.backgroundColor = .white
        
        dragView = UIView(frame:CGRect(x: 0, y: 0, width: 40, height: 8))
        dragView.layer.cornerRadius = 4
        dragView.autoresizingMask = [.flexibleLeftMargin]
        dragView.center = CGPoint(x: self.view.center.x, y: 0)
        dragView.backgroundColor = .darkGray
        self.view.addSubview(dragView)
        
        photoImageView = UIImageView(frame: self.view.bounds)
        photoImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        photoImageView.contentMode = .scaleAspectFill
        photoImageView.clipsToBounds = true
        photoImageView.kf.indicatorType = .activity
        photoImageView.alpha = 0.2
        self.view.addSubview(photoImageView)
        
        if let safeURL = article.photoURL {
            photoImageView.kf.setImage(with: URL(string: safeURL))
        }
        
        buildAttributedStrings()
        
        infoLabel = PaddingLabel(frame: CGRect(x: 0, y: 16, width: self.view.frame.width, height: 60))
        infoLabel.textInsets = UIEdgeInsetsMake(16, 16, 8, 16)
        infoLabel.numberOfLines = 2
        infoLabel.attributedText = attributedBottomLabelString
        infoLabel.textAlignment = .right
        self.view.addSubview(infoLabel)
        
        titleTextView = UITextView(frame: CGRect(x: 0, y: infoLabel.frame.height, width: self.view.frame.width, height: self.view.frame.height - infoLabel.frame.height - infoLabel.frame.origin.y))
        titleTextView.autoresizingMask = [.flexibleHeight, .flexibleWidth, .flexibleBottomMargin]
        titleTextView.attributedText = attributedMainLabelString
        titleTextView.isEditable = false
        titleTextView.isSelectable = false
        titleTextView.contentInset = UIEdgeInsetsMake(0, 32, 0, 32)
        titleTextView.backgroundColor = .clear
        self.view.addSubview(titleTextView)
        
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(sender:)))
        self.view.addGestureRecognizer(panGesture)
    }
    
    func buildAttributedStrings() {
        let font:NSAttributedStringKey = NSAttributedStringKey.font
        
        let titleAttributes:[NSAttributedStringKey:Any] = [
            font:UIFont.systemFont(ofSize: 28, weight: .medium),
            ]
        
        let subtitleAttributes:[NSAttributedStringKey:Any] = [
            font:UIFont.systemFont(ofSize: 17, weight: .regular),
            ]
        
        let infoLabelAttributes:[NSAttributedStringKey:Any] = [
            font:UIFont.systemFont(ofSize: 14, weight: .light),
            NSAttributedStringKey.foregroundColor:UIColor.darkGray,
            ]
        
        attributedMainLabelString = NSMutableAttributedString()
        attributedMainLabelString.append(NSAttributedString(string: article.title, attributes: titleAttributes))
        attributedMainLabelString.append(NSAttributedString(string: "\n\n", attributes: titleAttributes))
        
        if let safePerex = article.perex {
            attributedMainLabelString.append(NSAttributedString(string: safePerex, attributes: subtitleAttributes))
        }
        
        attributedBottomLabelString = NSMutableAttributedString()
        if let safeAuthorName = article.authorName {
            attributedBottomLabelString.append(NSAttributedString(string: safeAuthorName + ", ", attributes: infoLabelAttributes))
        }
        attributedBottomLabelString.append(NSAttributedString(string: article.sourceName, attributes: infoLabelAttributes))
        attributedBottomLabelString.append(NSAttributedString(string: " · ", attributes: infoLabelAttributes))
        attributedBottomLabelString.append(NSAttributedString(string: article.stringDurationUntilNow(), attributes: infoLabelAttributes))
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        infoLabel.frame.origin.y += self.view.safeAreaInsets.top
        titleTextView.frame = CGRect(x: 0, y: infoLabel.frame.height + infoLabel.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height - (infoLabel.frame.height + infoLabel.frame.origin.y) - 80)
        titleTextView.contentSize = CGSize(width: titleTextView.contentSize.width, height: titleTextView.sizeThatFits(titleTextView.frame.size).height)
        
        dragView.center = CGPoint(x: self.view.center.x, y: 8 + self.view.safeAreaInsets.top)
        
    }
    
    @objc func handlePan(sender:UIPanGestureRecognizer) {
        
        let percentThreshold:CGFloat = 0.3
        
        // convert y-position to downward pull progress in %
        let translation = sender.translation(in: view)
        let verticalMovement = translation.y / view.bounds.height
        let downwardMovement = fmaxf(Float(verticalMovement), 0.0)
        let downwardMovementPercent = fminf(downwardMovement, 1.0)
        let progress = CGFloat(downwardMovementPercent)
        
        guard let i = interactor else { return }
        
        switch sender.state {
        case .began:
            i.hasStarted = true
            dismiss(animated: true, completion: nil)
        case .changed:
            i.shouldFinish = progress > percentThreshold
            i.update(progress)
        case .cancelled:
            i.hasStarted = false
            i.cancel()
        case .ended:
            i.hasStarted = false
            i.shouldFinish
                ? i.finish()
                : i.cancel()
        default:
            break
        }
    }
}
