//
//  Article.swift
//  SimpleRRS
//
//  Created by marek on 26/06/2018.
//  Copyright © 2018 stana. All rights reserved.
//

import Foundation


class Article : NSObject {
    
    // MARK: - Properties
    
    var title:String!
    var perex:String?
    var URL:String!
    var photoURL:String?
    var publishedAt:Date!
    var sourceName:String!
    var sourceID:String?
    var authorName:String?
    
    var hasPhoto:Bool {
        get {
            return photoURL != nil
        }
    }
    
    // MARK: - Initialization
    
    init(withDictionary:NSDictionary) {
        super.init()
        
        title = withDictionary["title"] as? String
        perex = withDictionary["description"] as? String
        URL = withDictionary["url"] as? String
        photoURL = withDictionary["urlToImage"] as? String
        authorName = withDictionary["author"] as? String
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let dateString = withDictionary["publishedAt"] as? String
        publishedAt = formatter.date(from: dateString!)
        
        guard let sourceDictionary = withDictionary["source"] as? [AnyHashable: Any] else {
            return
        }
        
        sourceName = sourceDictionary["name"] as? String
        sourceID = sourceDictionary["ID"] as? String
        
    }
    
    override var description : String {
        return "[Article]" + "\ntitle:" + title + "\nURL:" + URL
    }   
    
}

extension Article {
    func stringDurationUntilNow() -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.day, .hour, .minute, .second]
        formatter.unitsStyle = .abbreviated
        formatter.maximumUnitCount = 1
        
        return formatter.string(from: Date().timeIntervalSince(publishedAt))!
    }
}

struct Constants {
    static let userCountry = "userCountry"
}
