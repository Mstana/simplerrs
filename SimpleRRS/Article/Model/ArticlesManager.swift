//
//  ArticlesManager.swift
//  SimpleRRS
//
//  Created by marek on 26/06/2018.
//  Copyright © 2018 stana. All rights reserved.
//

import Foundation
import Alamofire

class ArticlesManager {

    // MARK: - Properties
    private static let kAPIKEY:String = "2e1b73b53b324748a0f47cb0334868e9"
    private static let kVERSION:String = "/v2"
    private static let kBASEURL:String = "https://newsapi.org" + ArticlesManager.kVERSION
    
    
    // MARK: - Initialization
    private init() {}
    
    
    // MARK: - Accessors
    static let shared = ArticlesManager()
    
    
    // MARK: - Functions
    
    /**
     Fetch articles from API.
     At least one parameter of country || category has to be provided.
     Articles limit
     
     - Parameters:
     - forCountry: Specify articles country
     - category: Specify articles category
     - page: Index of page on API (indexed from 1)
     
     - Returns: Array of Articles
     */
    func fetchTopArticlesForCountry(_ country:String?, category:NewsCagetory = .general, page:Int = 1, complition: @escaping ([Article], Error?) -> ()) {
        let headers:HTTPHeaders = ["X-Api-Key": ArticlesManager.kAPIKEY]
        
        let stringRequest = urlBuilder(country: country, category: category, page: page)
        
        Alamofire.request(stringRequest, headers:headers).responseJSON { response in
           
            switch (response.result) {
            case .success(let JSON):
#if DEBUG
                print("[API Request]" + (response.request?.url?.absoluteString)!)
#endif
                guard let dictResult = JSON as? NSDictionary else {
                    return
                }
                guard dictResult["status"] as? String == "ok" else {
                    return // TODO-API: Inform API that return 200 with failing parameters
                }
                
                var articles : [Article] = Array()
                for d in dictResult["articles"] as! [Any] {
                    articles.append(Article(withDictionary: (d as? NSDictionary)!))
                }
                
                complition(articles, nil)
                break;
            case .failure(let error):
                complition(Array(), error)
                break;
            }
        }
    }
    
    // MARK: - Helpers
    private func urlBuilder(country:String?, category:NewsCagetory = .general, page:Int = 1) -> String {
        
        var stringRequest = ArticlesManager.kBASEURL + "/top-headlines?"
        stringRequest.append("pageSize=20")
        
        // API allows you to go without category but it will return results for default cat
        stringRequest.append("&category=" + category.rawValue)
        
        if let safeCountry = country {
            if  country != "" {
                stringRequest.append("&country=" + safeCountry)
            }
        }
        
        if page > 0 {
            stringRequest.append("&page=" + String(page))
        }
        
        return stringRequest
    }
}
