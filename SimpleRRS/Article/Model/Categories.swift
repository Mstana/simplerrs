//
//  Categories.swift
//  SimpleRRS
//
//  Created by marek on 26/06/2018.
//  Copyright © 2018 stana. All rights reserved.
//

import Foundation

enum NewsCagetory : String {
    
    case general            = "general"
    case business           = "business"
    case sports             = "sports"
    case health             = "health"
    case entertainment      = "entertainment"
    case science            = "science"
    case technology         = "technology"
    
    static let values = [
        general,
        business,
        sports,
        health,
        entertainment,
        science,
        technology
    ]
}
