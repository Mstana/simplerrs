//
//  UserSettings.swift
//  SimpleRRS
//
//  Created by marek on 28/06/2018.
//  Copyright © 2018 stana. All rights reserved.
//

import Alamofire

class UserSettings {
    
    // MARK: - Properties
    
    static let shared = UserSettings()
    
    var supportedCountries:[String] {
        get {
            return [
                "ae", "ar", "at", "au", "be","bg", "br", "ca", "ch",
                "cn", "co", "cu", "cz", "de", "eg", "fr", "gb", "gr",
                "hk", "hu", "id", "ie", "il", "in", "it", "jp", "kr",
                "lt", "lv", "ma", "mx", "my", "ng", "nl", "no", "nz",
                "ph", "pl", "pt", "ro", "rs", "ru", "sa", "se", "sg",
                "si", "sk", "th", "tr", "tw", "ua", "us", "ve", "za",
                ]
        }
    }
    
    var country:String {
        get {
            var lang:String = "us"
           
            if let userLanguage = UserDefaults.standard.string(forKey: Constants.userCountry) {
                lang = userLanguage
            } else if let safeUserRegionCode = NSLocale.current.regionCode {
                // set language from user's locale - on fisrt start of app
                if supportedCountries.contains(safeUserRegionCode.lowercased()) {
                    lang = safeUserRegionCode.lowercased()
                }
            }
            
            return lang
        }
    }
    
    // Initialization
    
    private init() {}
    
    // Functions
    
    func titleFor(newsCategory:NewsCagetory) -> String
    {
        switch newsCategory {
        case .business:
            return NSLocalizedString("Business", comment: "Category title")
        case .entertainment:
            return NSLocalizedString("Entertainment", comment: "Category title")
        case .general:
            return NSLocalizedString("General", comment: "Category title")
        case .health:
            return NSLocalizedString("Health", comment: "Category title")
        case .science:
            return NSLocalizedString("Science", comment: "Category title")
        case .sports:
            return NSLocalizedString("Sports", comment: "Category title")
        case .technology:
            return NSLocalizedString("Technology", comment: "Category title")
        }
    }
    
    func iconNameForCountry(name:String) -> String {
        return "CountryPicker.bundle/" + name.uppercased()
    }
}
