//
//  ArticleTableViewCell.swift
//  SimpleRRS
//
//  Created by marek on 28/06/2018.
//  Copyright © 2018 stana. All rights reserved.
//

import UIKit


protocol ArticleTableViewCellDelegate: class {
    func articleCellRequestedActionForArticle(_ article: Article?)
}

enum ArticleTableViewCellState {
    case title
    case perex
}

class ArticleTableViewCell : UITableViewCell {
    
    var article:Article! {
        didSet {
            refresh()
        }
    }
    
    var contentHolder: UIScrollView!
    var photoImageView: UIImageView!
    var photoGradient: CAGradientLayer!
    var titleLabel: PaddingLabel!
    var perexLabel: UITextView!
    var tapGesture: UITapGestureRecognizer!
    var cellState: ArticleTableViewCellState!
    
    weak var delegate: ArticleTableViewCellDelegate?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        cellState = .title
        self.backgroundColor = .black
        
        contentHolder = UIScrollView(frame: self.contentView.bounds)
        contentHolder.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentHolder.contentSize = CGSize(width: self.contentHolder.frame.width*2, height: self.contentHolder.frame.height)
        contentHolder.isPagingEnabled = true
        contentHolder.showsHorizontalScrollIndicator = false
        contentHolder.showsVerticalScrollIndicator = false
        contentHolder.canCancelContentTouches = false
        contentHolder.delegate = self
        self.contentView.addSubview(contentHolder)
        
        photoImageView = UIImageView(frame: self.contentView.bounds)
        photoImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        photoImageView.contentMode = .scaleAspectFill
        photoImageView.clipsToBounds = true
        photoImageView.kf.indicatorType = .activity
        self.contentView.insertSubview(photoImageView, belowSubview: contentHolder)
        
        photoGradient = CAGradientLayer()
        photoGradient.frame = photoImageView.bounds
        photoGradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        photoGradient.locations = [0.5, 1.0]
        photoImageView.layer.addSublayer(photoGradient)
        
        titleLabel = PaddingLabel(frame: self.contentView.bounds)
        titleLabel.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        titleLabel.numberOfLines = 3
        titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        titleLabel.textColor = .white
        titleLabel.textInsets = UIEdgeInsetsMake(8, 16, 8, 16)
        contentHolder.addSubview(titleLabel)
        
        perexLabel = UITextView(frame: self.contentView.bounds)
        perexLabel.frame.origin.x = titleLabel.frame.width
        perexLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        perexLabel.contentInset = UIEdgeInsetsMake(8, 16, 8, 16)
        perexLabel.font = UIFont.boldSystemFont(ofSize: 20)
        perexLabel.textColor = .black
        perexLabel.isEditable = false
        perexLabel.isSelectable = false
        perexLabel.showsVerticalScrollIndicator = false
        perexLabel.showsHorizontalScrollIndicator = false
        perexLabel.backgroundColor = UIColor.white.withAlphaComponent(0.68)
        perexLabel.canCancelContentTouches = false
        contentHolder.addSubview(perexLabel)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(contentTapAction(sender:)))
        contentHolder.addGestureRecognizer(tapGesture)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func refresh() {
        
        titleLabel.text = article.title
        titleLabel.sizeToFit()
        if let safePhotoURL = article.photoURL {
            photoImageView.kf.setImage(with: URL(string: safePhotoURL), placeholder: UIImage(named:"article-background-placeholder")!, options: [.transition(.fade(0.35))], progressBlock: nil)
        } else {
            photoImageView.image = UIImage(named:"article-background-placeholder")!
        }
        setPerexAttributedText()
    }
    
    func setPerexAttributedText() {
        
        let finalString = NSMutableAttributedString()
        
        if let safePerex = article.perex {
        
            let att:[NSAttributedStringKey:Any] = [
                NSAttributedStringKey.font : UIFont.systemFont(ofSize: 18, weight: .medium),
                NSAttributedStringKey.foregroundColor : UIColor.black,
            ]
            
            finalString.append(NSAttributedString(string: safePerex, attributes: att))
            
            
            contentHolder.isScrollEnabled = true
        } else {
            contentHolder.isScrollEnabled = false
        }
        
        
        if let safeAuthor = article.authorName {
            let att:[NSAttributedStringKey:Any] = [
                NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16, weight: .semibold),
                NSAttributedStringKey.foregroundColor : UIColor.darkGray,
            ]
            
            finalString.append(NSAttributedString(string: "\n\n\n" + safeAuthor, attributes: att))
        }
        
        let att:[NSAttributedStringKey:Any] = [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16, weight: .semibold),
            NSAttributedStringKey.foregroundColor : UIColor.darkGray,
            ]
        
        finalString.append(NSAttributedString(string: "\n" + article.stringDurationUntilNow() + " " +  NSLocalizedString("ago", comment: "Article time information, format:__duration__ ago"), attributes: att))
    
        perexLabel?.attributedText = finalString
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        photoGradient.frame = photoImageView.bounds
        
        titleLabel.frame = CGRect(x: 0, y: contentHolder.frame.height - titleLabel.frame.height, width: contentHolder.frame.width, height: titleLabel.frame.height)
        perexLabel.frame.origin.x = titleLabel.frame.width
        contentHolder.contentSize = CGSize(width: titleLabel.frame.width + perexLabel.frame.width, height: contentHolder.frame.height)
    }
    
    @objc func contentTapAction(sender:UITapGestureRecognizer) {
        delegate?.articleCellRequestedActionForArticle(article)
    }
}

extension ArticleTableViewCell : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView == contentHolder) {
            if scrollView.contentOffset.x == contentHolder.contentOffset.x {
                cellState = .perex
            } else {
                cellState = .title
            }
        }
    }
}
