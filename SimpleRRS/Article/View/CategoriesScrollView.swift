//
//  CategoriesScrollView.swift
//  SimpleRRS
//
//  Created by marek on 30/06/2018.
//  Copyright © 2018 stana. All rights reserved.
//

import UIKit

class CategoryButton : UIButton {
    var category:NewsCagetory?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setTitleColor(.white, for: .normal)
        setTitleColor(.blue, for: .selected)
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 5)
        layer.shadowRadius = 1
        
        clipsToBounds = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

protocol CategoriesScrollViewDelegate: class {
    func categoriesScrollViewDidSelectNewsCategory(_ category: NewsCagetory)
}

class CategoriesScrollView : UIScrollView {
    
    var categories: [NewsCagetory]! {
        didSet {
            refresh()
        }
    }
    
    var selected: NewsCagetory! {
        didSet {
            refresh()
        }
    }
    
    weak var categoriesDelegate: CategoriesScrollViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .black
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        categories = []
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func refresh() {
        
        for view in self.subviews {
            view.removeFromSuperview()
        }
        
        let padding = CGFloat(18)
        var left = padding
        for category in categories {
            
            if category == selected {
                continue
            }
            
            let b = CategoryButton(frame: CGRect(x: left, y: 0, width: 44, height: self.frame.height))
            b.autoresizingMask = [.flexibleWidth]
            b.setTitle(UserSettings.shared.titleFor(newsCategory: category), for: .normal)
            b.sizeToFit()
            b.category = category
            b.addTarget(self, action: #selector(buttonTapped(sender: )), for: .touchUpInside)
            b.center = CGPoint(x: b.center.x, y: self.frame.height/2)
            self.addSubview(b)
            left += (b.frame.width + padding)
        }
        
        self.contentSize = CGSize(width: left, height: self.contentSize.height)
        self.contentOffset = .zero
    }
    
    @objc func buttonTapped(sender:CategoryButton) {
        categoriesDelegate?.categoriesScrollViewDidSelectNewsCategory(sender.category!)
        selected = sender.category
    }
}
