//
//  PaddingLabel.swift
//  SimpleRRS
//
//  Created by marek on 28/06/2018.
//  Copyright © 2018 stana. All rights reserved.
//

import UIKit

class PaddingLabel : UILabel {
    
    var textInsets : UIEdgeInsets! {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.textInsets = UIEdgeInsets.zero
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        
        
        var rect = super.textRect(forBounds: UIEdgeInsetsInsetRect(bounds, textInsets), limitedToNumberOfLines: numberOfLines)
        
        rect.origin.x    -= textInsets.left
        rect.origin.y    -= textInsets.top
        rect.size.width  += (textInsets.left + textInsets.right)
        rect.size.height += (textInsets.top + textInsets.bottom)
        
        return rect
    }
    
    override func draw(_ rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, textInsets))
    }
}
